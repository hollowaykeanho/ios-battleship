//
//  SeaGridViewController.m
//  Battleship
//
//  Created by Kean Ho Chew on 30/03/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import "SeaGridViewController.h"

static NSInteger maxBoundary = 5;
static NSInteger totalShipCells = 6;

enum cellType {
    NONE = 0,
    GUNBOAT,
    DESTROYER,
    CRUISER,
};

enum playerList {
    PLAYER_1 = 1,
    PLAYER_2
};

enum gameStatus {
    BUSY = 0,
    PLAYER_1_MAP_SETUP,
    PLAYER_2_MAP_SETUP,
    PLAYER_1_HIT,
    PLAYER_2_HIT,
    WINNER
};


@interface SeaGridViewController ()
@property (weak, nonatomic) IBOutlet UILabel *actionLabel;
@property (weak, nonatomic) IBOutlet UIButton *attackButton;
@property (weak, nonatomic) IBOutlet UIButton *mapButton;

@property (weak, nonatomic) IBOutlet UIButton *button00;
@property (weak, nonatomic) IBOutlet UIButton *button01;
@property (weak, nonatomic) IBOutlet UIButton *button02;
@property (weak, nonatomic) IBOutlet UIButton *button03;
@property (weak, nonatomic) IBOutlet UIButton *button04;
@property (weak, nonatomic) IBOutlet UIButton *button10;
@property (weak, nonatomic) IBOutlet UIButton *button11;
@property (weak, nonatomic) IBOutlet UIButton *button12;
@property (weak, nonatomic) IBOutlet UIButton *button13;
@property (weak, nonatomic) IBOutlet UIButton *button14;
@property (weak, nonatomic) IBOutlet UIButton *button20;
@property (weak, nonatomic) IBOutlet UIButton *button21;
@property (weak, nonatomic) IBOutlet UIButton *button22;
@property (weak, nonatomic) IBOutlet UIButton *button23;
@property (weak, nonatomic) IBOutlet UIButton *button24;
@property (weak, nonatomic) IBOutlet UIButton *button30;
@property (weak, nonatomic) IBOutlet UIButton *button31;
@property (weak, nonatomic) IBOutlet UIButton *button32;
@property (weak, nonatomic) IBOutlet UIButton *button33;
@property (weak, nonatomic) IBOutlet UIButton *button34;
@property (weak, nonatomic) IBOutlet UIButton *button40;
@property (weak, nonatomic) IBOutlet UIButton *button41;
@property (weak, nonatomic) IBOutlet UIButton *button42;
@property (weak, nonatomic) IBOutlet UIButton *button43;
@property (weak, nonatomic) IBOutlet UIButton *button44;

- (IBAction)buttonPressed:(id)sender;
- (IBAction)mapButtonPressed:(id)sender;
- (IBAction)attackButtonPressed:(id)sender;
@end


@implementation SeaGridViewController
{
    UIButton *seaGridButtons[5][5];
    NSString *seaGridLabels[5];
    
    NSString *player1Map[5][5];
    BOOL player1HitMap[5][5];
    NSInteger player1RemainingShipCell;
    
    NSString *player2Map[5][5];
    BOOL player2HitMap[5][5];
    NSInteger player2RemainingShipCell;
    
    NSInteger currentPlayer;
    NSInteger currentStatus;
    NSInteger winner;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self hardSetup];
    [self softReset];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)buttonPressed:(id)sender
{
    UIButton *button = sender;
    int horizontal = 0;
    int vertical = 0;
    [self convertToCoordinate:button.accessibilityIdentifier yAxis:&horizontal xAxis:&vertical];
    
    if (currentPlayer == PLAYER_1) {
        player1HitMap[horizontal][vertical] = YES;
        currentPlayer = PLAYER_2;
        [self setGameActionLabel:PLAYER_2_HIT];
        if (![player2Map[horizontal][vertical] isEqualToString:seaGridLabels[NONE]]) {
            player2RemainingShipCell -= 1;
        }
        if (player2RemainingShipCell <= 0) {
            [self showWinner:PLAYER_1];
        }
        [self showTurn:PLAYER_2];
        
    } else {
        player2HitMap[horizontal][vertical] = YES;
        currentPlayer = PLAYER_1;
        [self setGameActionLabel:PLAYER_1_HIT];
        if (![player1Map[horizontal][vertical] isEqualToString:seaGridLabels[NONE]]) {
            player1RemainingShipCell -= 1;
        }
        if (player1RemainingShipCell <= 0) {
            [self showWinner:PLAYER_2];
        }
        [self showTurn:PLAYER_1];
    }
    
    [self updateHitCells];
}

- (IBAction)mapButtonPressed:(id)sender
{
    [self updateShipCells];
}

- (IBAction)attackButtonPressed:(id)sender
{
    [self updateHitCells];
}

- (void)showTurn:(int)player
{
    UIAlertController *alert;
    NSString *message;
    
    switch (player) {
        case PLAYER_1:
            message = @"Player 1 Turn";
            break;
        case PLAYER_2:
            message = @"Player 2 Turn";
            break;
    }
    
    alert = [UIAlertController alertControllerWithTitle:@"Turns" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleCancel
                                               handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showWinner:(int)player
{
    UIAlertController *alert;
    NSString *message;
    
    switch(player) {
        case PLAYER_1:
            message = @"Player 1 Win!";
            break;
        case PLAYER_2:
            message = @"Player 2 Win!";
            break;
    }
    
    
    
    alert = [UIAlertController alertControllerWithTitle:@"Winner" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *try_again = [UIAlertAction actionWithTitle:@"Try Again"
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction * action){
                                                          [self softReset];
    }];
    
    [alert addAction:try_again];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)convertToCoordinate:(NSString *)identifier yAxis:(int *)yAxis xAxis:(int *)xAxis
{
    NSString *singleChar;
    
    singleChar = [identifier substringWithRange:NSMakeRange(0, 1)];
    *yAxis = [singleChar integerValue];
    singleChar = [identifier substringWithRange:NSMakeRange(1, 1)];
    *xAxis = [singleChar integerValue];
}


#pragma mark - UpdateShipCell
- (void)updateShipCells
{
    int vertical = 0;
    int horizontal = 0;
    
    // Reset all background color to white color
    for (horizontal = 0; horizontal < maxBoundary; horizontal++) {
        for (vertical = 0; vertical < maxBoundary; vertical++) {
            seaGridButtons[horizontal][vertical].backgroundColor = [UIColor whiteColor];
            seaGridButtons[horizontal][vertical].enabled = NO;
            
            if (currentPlayer == PLAYER_1) {
                [seaGridButtons[horizontal][vertical]
                 setTitle:player1Map[horizontal][vertical]
                 forState:UIControlStateNormal];
                
                if (player2HitMap[horizontal][vertical] == YES) {
                    if (![player1Map[horizontal][vertical] isEqualToString:seaGridLabels[NONE]]) {
                        seaGridButtons[horizontal][vertical].backgroundColor = [UIColor redColor];
                    } else {
                        seaGridButtons[horizontal][vertical].backgroundColor = [UIColor greenColor];
                    }
                } else {
                    seaGridButtons[horizontal][vertical].backgroundColor = [UIColor whiteColor];
                }
                
            } else if (currentPlayer == PLAYER_2) {
                
                [seaGridButtons[horizontal][vertical]
                 setTitle:player2Map[horizontal][vertical]
                 forState:UIControlStateNormal];
                
                if (player1HitMap[horizontal][vertical] == YES) {
                    if (![player2Map[horizontal][vertical] isEqualToString:seaGridLabels[NONE]]) {
                        seaGridButtons[horizontal][vertical].backgroundColor = [UIColor redColor];
                    } else {
                        seaGridButtons[horizontal][vertical].backgroundColor = [UIColor greenColor];
                    }
                } else {
                    seaGridButtons[horizontal][vertical].backgroundColor = [UIColor whiteColor];
                }
            }
        }
    }
}


- (void)updateHitCells
{
    int vertical = 0;
    int horizontal = 0;
    
    if (currentPlayer == PLAYER_1) {
        for (horizontal = 0; horizontal < maxBoundary; horizontal++) {
            for (vertical = 0; vertical < maxBoundary; vertical++) {
                [seaGridButtons[horizontal][vertical] setTitle:seaGridLabels[NONE] forState:UIControlStateNormal];
                seaGridButtons[horizontal][vertical].enabled = YES;
                
                if (player1HitMap[horizontal][vertical] == YES) {
                    seaGridButtons[horizontal][vertical].enabled = NO;
                    if (![player2Map[horizontal][vertical] isEqualToString:seaGridLabels[NONE]]) {
                        seaGridButtons[horizontal][vertical].backgroundColor = [UIColor redColor];
                    } else {
                        seaGridButtons[horizontal][vertical].backgroundColor = [UIColor yellowColor];
                    }
                } else {
                    seaGridButtons[horizontal][vertical].backgroundColor = [UIColor greenColor];
                }
            }
        }
        
    } else {
        for (horizontal = 0; horizontal < maxBoundary; horizontal++) {
            for (vertical = 0; vertical < maxBoundary; vertical++) {
                [seaGridButtons[horizontal][vertical] setTitle:seaGridLabels[NONE] forState:UIControlStateNormal];
                seaGridButtons[horizontal][vertical].enabled = YES;
                
                if (player2HitMap[horizontal][vertical] == YES) {
                    seaGridButtons[horizontal][vertical].enabled = NO;
                    if (![player1Map[horizontal][vertical] isEqualToString:seaGridLabels[NONE]]) {
                        seaGridButtons[horizontal][vertical].backgroundColor = [UIColor redColor];
                    } else {
                        seaGridButtons[horizontal][vertical].backgroundColor = [UIColor yellowColor];
                    }
                } else {
                    seaGridButtons[horizontal][vertical].backgroundColor = [UIColor greenColor];
                }
            }
        }
    }
}


#pragma mark - ActionLabel
- (void)setGameActionLabel:(NSInteger)gameStatus
{
    switch (gameStatus) {
        case PLAYER_1_HIT:
            self.actionLabel.text = @"Player 1 - Yours to Hit";
            break;
        case PLAYER_2_HIT:
            self.actionLabel.text = @"Player 2 - Yours to Hit";
            break;
        case WINNER:
            if (winner == PLAYER_1) {
                self.actionLabel.text = @"Player 1 WIN!";
            } else if (winner == PLAYER_2) {
                self.actionLabel.text = @"Player 2 WIN";
            } else {
                self.actionLabel.text = @"Something is Wrong!";
            }
            break;
        default:
            break;
    }
}


#pragma mark - Setup Views
- (void)generatePlayer1RandomBoard:(int)boardId
{
    int horizontal = 0;
    int vertical = 0;
    
    switch(boardId) {
        case 0:
            // N N N G N
            // N D N N N
            // N D N N N
            // N N N N N
            // N C C C N
            for (horizontal = 0; horizontal < maxBoundary; horizontal++) {
                for (vertical = 0; vertical < maxBoundary; vertical++) {
                    if (horizontal == 0 && vertical == 3) {
                        player1Map[horizontal][vertical] = seaGridLabels[GUNBOAT];
                    } else if ((horizontal == 1 && vertical == 1) ||
                               (horizontal == 2 && vertical == 1)) {
                        player1Map[horizontal][vertical] = seaGridLabels[DESTROYER];
                    } else if ((horizontal == 4 && vertical == 1) ||
                               (horizontal == 4 && vertical == 2) ||
                               (horizontal == 4 && vertical == 3)) {
                        player1Map[horizontal][vertical] = seaGridLabels[CRUISER];
                    } else {
                        player1Map[horizontal][vertical] = seaGridLabels[NONE];
                    }
                }
            }
            break;
        default:
            break;
    }
}

- (void)generatePlayer2RandomBoard:(int)boardId
{
    int horizontal = 0;
    int vertical = 0;
    
    switch(boardId) {
        case 0:
            // N D D N N
            // N N N N N
            // N C N G N
            // N C N N N
            // N C N N N
            for (horizontal = 0; horizontal < maxBoundary; horizontal++) {
                for (vertical = 0; vertical < maxBoundary; vertical++) {
                    if (horizontal == 2 && vertical == 3) {
                        player2Map[horizontal][vertical] = seaGridLabels[GUNBOAT];
                    } else if ((horizontal == 0 && vertical == 1) ||
                               (horizontal == 0 && vertical == 2)) {
                        player2Map[horizontal][vertical] = seaGridLabels[DESTROYER];
                    } else if ((horizontal == 2 && vertical == 1) ||
                               (horizontal == 3 && vertical == 1) ||
                               (horizontal == 4 && vertical == 1)) {
                        player2Map[horizontal][vertical] = seaGridLabels[CRUISER];
                    } else {
                        player2Map[horizontal][vertical] = seaGridLabels[NONE];
                    }
                }
            }
            break;
        default:
            break;
    }
}


- (void)softReset
{
    int vertical = 0;
    int horizontal = 0;
    
    
    // Reset all players map and hit tracking
    for (horizontal = 0; horizontal < maxBoundary; horizontal++) {
        for (vertical = 0; vertical < maxBoundary; vertical++) {
            player1Map[horizontal][vertical] = seaGridLabels[NONE];
            player1HitMap[horizontal][vertical] = NO;
            player2Map[horizontal][vertical] = seaGridLabels[NONE];
            player2HitMap[horizontal][vertical] = NO;
            
            [[seaGridButtons[horizontal][vertical] layer] setBorderWidth:2.0f];
            [seaGridButtons[horizontal][vertical] setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
            [[seaGridButtons[horizontal][vertical] layer] setBorderColor:[UIColor blackColor].CGColor];
        }
    }
    
    currentPlayer = PLAYER_1;
    player1RemainingShipCell = totalShipCells;
    player2RemainingShipCell = totalShipCells;
    
    [self setGameActionLabel:PLAYER_1_HIT];
    [self generatePlayer1RandomBoard:0];
    [self generatePlayer2RandomBoard:0];
    [self updateHitCells];
}

- (void)hardSetup
{
    seaGridButtons[0][0] = self.button00;
    seaGridButtons[0][1] = self.button01;
    seaGridButtons[0][2] = self.button02;
    seaGridButtons[0][3] = self.button03;
    seaGridButtons[0][4] = self.button04;
    
    seaGridButtons[1][0] = self.button10;
    seaGridButtons[1][1] = self.button11;
    seaGridButtons[1][2] = self.button12;
    seaGridButtons[1][3] = self.button13;
    seaGridButtons[1][4] = self.button14;
    
    seaGridButtons[2][0] = self.button20;
    seaGridButtons[2][1] = self.button21;
    seaGridButtons[2][2] = self.button22;
    seaGridButtons[2][3] = self.button23;
    seaGridButtons[2][4] = self.button24;
    
    seaGridButtons[3][0] = self.button30;
    seaGridButtons[3][1] = self.button31;
    seaGridButtons[3][2] = self.button32;
    seaGridButtons[3][3] = self.button33;
    seaGridButtons[3][4] = self.button34;
    
    seaGridButtons[4][0] = self.button40;
    seaGridButtons[4][1] = self.button41;
    seaGridButtons[4][2] = self.button42;
    seaGridButtons[4][3] = self.button43;
    seaGridButtons[4][4] = self.button44;
    
    seaGridLabels[NONE] = @" ";
    seaGridLabels[GUNBOAT] = @"G";
    seaGridLabels[DESTROYER] = @"D";
    seaGridLabels[CRUISER] = @"C";
}
@end
