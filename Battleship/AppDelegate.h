//
//  AppDelegate.h
//  Battleship
//
//  Created by Kean Ho Chew on 30/03/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

